"use strict";

angular.module('app.admin', [ 'ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.admin', ['ui.router'])
    .config(function ($stateProvider) {

        $stateProvider
            .state('app.admin', {
                url: '/admin',
                data: {
                    title: 'Admin'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/admin/views/admin.html',
                        controller: 'AdminCtrl'
                    }
                }
            })
   
    });
