"use strict";

angular.module('app.preferences', [ 'ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.preferences', ['ui.router'])
.config(function ($stateProvider) {

    $stateProvider
        .state('app.preferences', {
            url: '/preferences',
            data: {
                title: 'preferences'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/preferences/views/preference.html',
                    controller: 'PreferenceCtrl'
                }
            }
        })
});
