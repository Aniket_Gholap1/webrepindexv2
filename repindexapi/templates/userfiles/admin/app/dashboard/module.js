"use strict";


angular.module('app.dashboard', ['ui.router'])
.config(function ($stateProvider) {

    $stateProvider
        .state('app.dashboard', {
            url: '/dashboard',
            data: {
                title: 'Blank'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/dashboard/views/dashboard.html',
                    // controller: 'ConsumerCtrl'
                }
            }
        })
});
