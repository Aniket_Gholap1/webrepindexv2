"use strict";

angular.module('app.advertisement', ['ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.advertisement', ['ui.router'])
    .config(function ($stateProvider) {

        $stateProvider
            .state('app.advertisement', {
                url: '/advertisement',
                data: {
                    title: 'advertisement'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/advertisement.html',
                        controller: 'PreferenceCtrl'
                    }
                }
            })

            .state('app.planadvertisement', {
                url: '/planadvertisement',
                data: {
                    title: 'Plan'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/planadvertisement.html',
                        controller: 'PlanAdvCtrl'
                    }
                }
            })

            .state('app.planadvertisementedit', {
                url: '/planadvertisementedit',
                data: {
                    title: 'advertisement'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/planadvertisementedit.html',
                        controller: 'PreferenceCtrl'
                    }
                }
            })

            .state('app.myadvertisement', {
                url: '/myadvertisement',
                data: {
                    title: 'myadvertisement'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/myadvertisement.html',
                        controller: 'MyAdvertiseCtrl'
                    }
                }
            })

            .state('app.adddetails', {
                url: '/adddetails',
                data: {
                    title: 'adddetails'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/adddetails.html',
                        controller: 'AddDetailsCtrl'
                    }
                }
            })
            .state('app.addpost', {
                url: '/addpost',
                data: {
                    title: 'addpost'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/addpost.html',
                        controller: 'AddDetailsCtrl'
                    }
                }
            })
            .state('app.myposts', {
                url: '/myposts',
                data: {
                    title: 'myposts'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/myposts.html',
                        controller: 'AddDetailsCtrl'
                    }
                }
            })

            .state('app.messages', {
                url: '/messages',
                data: {
                    title: 'messages'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/messages.html',
                        controller: 'AddDetailsCtrl'
                    }
                }
            })

            .state('app.postedAids', {
                url: '/postedAids',
                data: {
                    title: 'postedAids'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/postedAids.html',
                        controller: 'AddDetailsCtrl'
                    }
                }
            })

            .state('app.reviews', {
                url: '/reviews',
                data: {
                    title: 'reviews'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/reviews.html',
                        controller: 'AddDetailsCtrl'
                    }
                }
            })

            .state('app.transaction', {
                url: '/transaction',
                data: {
                    title: 'transaction'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/advertisement/views/transaction.html',
                        controller: 'AddDetailsCtrl'
                    }
                }
            })
    });
