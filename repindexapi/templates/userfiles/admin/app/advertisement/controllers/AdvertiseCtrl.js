'use strict';

angular.module('app.advertisement').controller('AdvertiseCtrl', function ($scope, $state, $parse, DTOptionsBuilder) {

    // $scope.currentUser = JSON.parse(localStorage.getItem('userObject'));


        $scope.standardOptions = DTOptionsBuilder.newOptions()
            .withOption('bFilter', true)
            .withOption('bLengthChange', true)
            .withOption('bAutoWidth', false)
            .withOption('bPaginate', true)
            .withOption('bInfo', false)
            .withOption('bInfo', false)
            .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
            "t" +
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
            // .withDOM('frtrip')
            .withLanguage({
                "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
            })
            .withBootstrap();
})