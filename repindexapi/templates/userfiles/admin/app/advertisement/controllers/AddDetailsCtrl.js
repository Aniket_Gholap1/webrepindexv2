'use strict';

angular.module('app.advertisement').controller('AddDetailsCtrl', function ($scope, $state, $parse, DTOptionsBuilder) {

    // $scope.currentUser = JSON.parse(localStorage.getItem('userObject'));

    var mapHtml = '<iframe src="https://maps.google.com/maps?address=undefined&q=39.9129412,-104.7956055&t=m&hl=en;z=5&amp;output=embed" height="100%" width="100%" style="height:249px;"></iframe>'
    document.getElementById('googleMap').innerHTML = mapHtml;

    $scope.loadmap = function () {
      
    }


    $scope.standardOptions = DTOptionsBuilder.newOptions()
        .withOption('bFilter', true)
        .withOption('bLengthChange', true)
        .withOption('bAutoWidth', false)
        .withOption('bPaginate', true)
        .withOption('bInfo', false)
        .withOption('bInfo', false)
        .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
        // .withDOM('frtrip')
        .withLanguage({
            "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
        })
        .withBootstrap();
})