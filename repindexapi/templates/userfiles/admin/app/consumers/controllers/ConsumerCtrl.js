'use strict';

angular.module('app.consumers').controller('ConsumerCtrl', function ($scope, $state, $timeout, $filter, ConsumerResources, DTOptionsBuilder) {


    var loadRoles = function () {
        console.log("API to bring roles")
        var roleRequest = ConsumerResources.roles();
        roleRequest.success(function (roleResponse) {
            var roles = roleResponse.result;
            $scope.role = $filter('filter')(roles, { role: 'Consumer' })[0];
            // var id = $scope.role_id.id;
        })
    }

    loadRoles();

    var loadConsumers = function () {
        console.log("API to bring Consumers")
        var consumersRequest = ConsumerResources.consumers();
        consumersRequest.success(function (consumersResponse) {
            if (consumersResponse.success) {
                $scope.consumers = consumersResponse.result;
            }
            // else if ($scope.consumer.status == 'inactive') {
            //     $scope.inActiveUser = true;
            // }

        }
        )
    }
    loadConsumers();

    $scope.setConsumer = function (consumer, index) {
        $scope.currentConsumerIndex = index;
        $scope.currentConsumer = consumer;
    }

    $scope.changeStatus = function (consumer) {
        var query = {};
        query.id = consumer.id;
        var changeStatusRequest = ConsumerResources.changeStatus(query);
        changeStatusRequest.success(function (changeStatusResponse) {

            loadConsumers();
        })
    }

    $scope.emailVerify = function (consumer) {
        var query = {};
        query.id = consumer.id;
        var emailVerifyRequest = ConsumerResources.emailVerify(query);
        emailVerifyRequest.success(function (emailVerifyResponse) {

            loadConsumers();
        })
    }

    $scope.sendEmail = function (consumer) {
        var query = {};
        query.id = consumer.id;
        var sendEmailRequest = ConsumerResources.sendEmail(query);
        sendEmailRequest.success(function (sendEmailResponse) {
            if (sendEmailResponse.success) {
                $scope.result = sendEmailResponse.result;

            }
            else {
                $scope.result = sendEmailResponse.result;

            }
        })
    }

    $scope.addConsumer = function () {
        $scope.newConsumer = {};
        $('#addConsumerPopup').modal('show');
    }

    $scope.editConsumer = function () {
        if ($scope.currentConsumer != null) {
            $scope.newConsumer = $scope.currentConsumer;
            $('#addConsumerPopup').modal('show');
        }
        // else {
        //     $scope.modalInstance = $modal.open({
        //         templateUrl: 'app/modules/data-gov/views/noSelection.modal.view.html',
        //         controller: 'removeModalCtrl',
        //         controllerAs: 'vm',
        //         resolve: {
        //             model: function () { return 'Vendor' },
        //             action: function () { return 'Edit'; }
        //         }
        //     });
        // }
    }

    $scope.deleteConsumer = function () {
        if ($scope.currentConsumer != null) {
            $('#deleteConsumerPopup').modal('show');
        }
    }

    $scope.consumerDelete = function () {
        console.log("API to delete Consumer")
        var query = {};
        query.id = $scope.currentConsumer.id;
        var consumerRequest = ConsumerResources.deleteConsumer(query);
        consumerRequest.success(function (consumerResponse) {
            if (consumerResponse.success) {
                loadConsumers();
                $('#deleteConsumerPopup').modal('hide');
                $scope.success = true;
                $scope.consumers = consumerResponse.result;
            }
            else {
                $('#deleteConsumerPopup').modal('hide');
                $scope.failed = true;
            }
        })
    }

    $scope.submit = function (newConsumer) {
        if ($scope.consumerAction == 'Edit') {
            var editConsumerReq = ConsumerResources.editConsumer(newConsumer);
            editConsumerReq.success(function (editConsumerResponse) {
                if (editConsumerResponse.success) {
                    loadConsumer();
                    $scope.success = true;
                    $scope.result = editConsumerResponse.result;
                }
                else {
                    $scope.failed = true;
                }

            })
        }
        $scope.newConsumer = {};
        $scope.currentConsumerIndex = null;
        $('#addConsumerPopup').modal('hide');
    }

    $scope.cancel = function () {
        $scope.currentConsumer = null;
        $scope.newConsumer = {};
        $scope.currentConsumer = null;
        $('#addConsumerPopup').modal('hide');
        $('#deleteConsumerPopup').modal('hide');
    }


    $scope.standardOptions = DTOptionsBuilder.newOptions()
        .withOption('bFilter', true)
        .withOption('bLengthChange', true)
        .withOption('bAutoWidth', false)
        .withOption('bPaginate', true)
        .withOption('bInfo', false)
        .withOption('bInfo', false)
        .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
        // .withDOM('frtrip')
        .withLanguage({
            "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
        })
        .withBootstrap();
});