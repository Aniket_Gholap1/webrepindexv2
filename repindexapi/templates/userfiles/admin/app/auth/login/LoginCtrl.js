'use strict';

angular.module('app.auth').controller('LoginCtrl', function ($scope, $filter, $rootScope, $state, $location, LoginResource) {



    var url = $location.search()

    $scope.submitForm = function (LoginData) {
        var query = {};
        query.LoginData = LoginData;
        query.Token = url;
        console.log("I am entering the inside");
        var loginReq = LoginResource.login(query);
        loginReq.success(function (loginResponse) {
            if (loginResponse.success) {
                $rootScope.authenticateduser = true;
                $scope.user = loginResponse.result[0];
                $scope.permission = loginResponse.Permission;
                localStorage.setItem('permission', JSON.stringify($scope.permission));
                // localStorage.setItem('usertoken','');
                localStorage.setItem('userObject', JSON.stringify($scope.user));
                var user = $scope.user;
                $state.go('app.dashboard');
            }
            else {
                $scope.result = loginResponse.result
                $scope.authenticationFail = true;
            }

        })
    };
});
