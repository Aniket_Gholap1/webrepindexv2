"use strict";



angular.module('app.roles', ['ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.roles').config(function ($stateProvider) {

    $stateProvider
        .state('app.roles', {
            url: '/roles',
            data: {
                title: 'Roles'

            },
            views: {
                "content@app": {
                    templateUrl: 'app/roles/views/rolesTable.html',
                    controller: 'RoleCtrl'
                }
            }
        })
        .state('app.permission', {
            url: '/permission:obj1?role:obj2',
            data: {
                title: 'Permission'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/roles/views/permission.html',
                    controller: 'PermissionCtrl'
                },
            }
        })
});
