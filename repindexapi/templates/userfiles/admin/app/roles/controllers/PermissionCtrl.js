'use strict';

angular.module('app.roles').controller('PermissionCtrl', function (DTOptionsBuilder, $scope, $filter, $state, $stateParams, RoleResources) {

    $scope.id = $state.params.obj1;
    $scope.role = $state.params.obj2;
    var id = $state.params.obj1;
    var query = {};
    query.id = id;
    var permissionReq = RoleResources.permission(query);
    permissionReq.success(function (permissionResponse) {
        if (permissionResponse.success) {
            $scope.permission = permissionResponse.result;

            //             angular.forEach( $scope.permission,function(permission){
            // if(permission.0==true){

            // }
            //             })
            console.log("I am entering the inside");
        }
        else {
            $state.go('app.login');
        }
    })
    $scope.savePermission = function () {
        //    var verification = $filter('filter')(res, { id: parseInt($stateParams.verificationId) });
        var permission = $scope.permission;
        var id = $state.params.obj1;
        var verification = $filter('filter')(permission, { select: true });
        console.log(verification)
        if ($scope.permissionAction == 'Save') {
            var query = {};
            query.id = id;
            query.verification = verification;
            var savePermissionReq = RoleResources.savePermission(query);
            // savePermissionReq.success(function (savePermissionResponse) {
            //     if (savePermissionResponse.success) {
            $scope.success = true;
            $scope.result = rolePermission.result;
        }
        // else{
        //     $scope.failed = true;
        // }
        //     })
        // }
        // if ($scope.permissionAction == 'Add') {
        //     var savePermissionReq = RoleResources.savePermission(rolePermission);
        //     savePermissionReq.success(function (rolePermission) {
        //         $scope.result = rolePermission.result;
        //     })
        // }
    }


    $scope.standardOptions = DTOptionsBuilder.newOptions()
        .withOption('bFilter', true)
        .withOption('bLengthChange', true)
        .withOption('bAutoWidth', false)
        .withOption('bPaginate', true)
        .withOption('bInfo', false)
        .withOption('bInfo', false)
        .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
        // .withDOM('frtrip')
        .withLanguage({
            "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
        })
        .withBootstrap();
});