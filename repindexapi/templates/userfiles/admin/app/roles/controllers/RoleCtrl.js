'use strict';

angular.module('app.roles').controller('RoleCtrl', function (DTOptionsBuilder, $scope, $state, RoleResources) {

    var loadRoles = function () {
        console.log("API to bring roles")
        var roleRequest = RoleResources.roles();
        roleRequest.success(function (roleResponse) {
            $scope.roles = roleResponse.result;
        })
    }
    loadRoles();

    $scope.access = function (role) {
        $state.go('app.permission', { obj1:role.id,obj2:role.role});
    };

    $scope.setRole = function (role, index) {
        $scope.currentRoleIndex = index;
        $scope.currentRole = role;
    }

    $scope.addRole = function () {
        $scope.newRole = {};
        $('#addRolePopup').modal('show');
    }
    $scope.editRole = function () {
        if ($scope.currentRole != null) {
            $scope.newRole = $scope.currentRole;
            $('#addRolePopup').modal('show');
        }
    }
    $scope.deleteRole = function () {
        if ($scope.currentRole != null) {
            $('#deleteRolePopup').modal('show');
        }
    }
    $scope.roleDelete = function () {
        console.log("API to bring admins")
        var query = {};
        query.id = $scope.currentRole.id;
        var roleRequest = RoleResources.deleteRole(query);
        roleRequest.success(function (roleResponse) {
            if (roleResponse.success) {
                $('#deleteRolePopup').modal('hide');
                loadRoles();
                $scope.roles = roleResponse.result;
            }
        })
    }

    $scope.submit = function (newRole) {
        if ($scope.roleAction == 'Add') {
            var addRoleReq = RoleResources.addRole(newRole);
            addRoleReq.success(function (addRoleResponse) {
                if (addRoleResponse.success) {
                    loadRoles();
                    $scope.result = addRoleResponse.result;
                }
            })
        }
        if ($scope.roleAction == 'Edit') {
            var role = $scope.currentRole;
            var editRoleReq = RoleResources.editRole(role);
            editRoleReq.success(function (editRoleResponse) {
                if (editRoleResponse.success) {
                    $scope.success = true;
                    $scope.result = editRoleResponse;
                }
                else {
                    $scope.failed = true;
                    // $scope.result = editRoleResponse;
                }
            })
        }
    }
    $scope.cancel = function () {
        $scope.currentAdmin = null;
        $scope.newAdmin = {};
        $scope.currentAdmin = null;
        $('#deleteRolePopup').modal('hide');
        $('#deleteRolePopup').modal('hide');
    }

    $scope.standardOptions = DTOptionsBuilder.newOptions()
        .withOption('bFilter', true)
        .withOption('bLengthChange', true)
        .withOption('bAutoWidth', false)
        .withOption('bPaginate', true)
        .withOption('bInfo', false)
        .withOption('bInfo', false)
        .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
        // .withDOM('frtrip')
        .withLanguage({
            "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
        })
        .withBootstrap();
});