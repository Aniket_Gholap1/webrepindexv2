"use strict";

angular.module('app.profile', [ 'ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.profile', ['ui.router'])
.config(function ($stateProvider) {

    $stateProvider
        .state('app.profile', {
            url: '/profile',
            data: {
                title: 'profile'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/profile/views/profile.html',
                    controller: 'ProfileCtrl'
                }
            }
        })
});
