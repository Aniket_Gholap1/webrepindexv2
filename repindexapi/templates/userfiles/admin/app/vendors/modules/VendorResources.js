

'use strict';

angular.module('app.vendors').factory('VendorResources', function ($http, $q, APP_CONFIG, httpApiConfig) {
    var dfd = $q.defer();

    return {
        // get_Admins: $resource(APP_CONFIG.apiRootUrl + "/Admin/adminApi", {}, {
        //     'query': { method: 'get', isArray: true },
        //     'add': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/create', isArray: true },
        //     'delete': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/delete' },
        //     'update': { method: 'POST', url: apiUrl.url + '/Admin/adminApi/update' }
        // }),
        roles: function () {
            var ROLE_URL = APP_CONFIG.apiRootUrl + "/Roles/roleApi";
            return $http.get(ROLE_URL);
        },
        vendors: function () {
            var VENDOR_URL = APP_CONFIG.apiRootUrl + "/vendor";
            return $http.get(VENDOR_URL);
        },

        addVendor: function (query) {
            var ADD_URL = APP_CONFIG.apiRootUrl + "/vendor";
            return $http.post(ADD_URL, query, httpApiConfig);
        },
        editVendor: function (query) {
            var UPDATE_URL = APP_CONFIG.apiRootUrl + "/vendor/" + query.id;
            return $http.put(UPDATE_URL, query, httpApiConfig);
        },
        deleteVendor: function (query) {
            var DELETE_URL = APP_CONFIG.apiRootUrl + "/vendor/" + query.id;
             return $http.delete(DELETE_URL, query, httpApiConfig);
        },
        changeStatus: function (query) {
            var STATUS_URL = APP_CONFIG.apiRootUrl + "/Register/changeStatus";
            return $http.post(STATUS_URL, query, httpApiConfig);
        },
        emailVerify: function (query) {
            var STATUS_URL = APP_CONFIG.apiRootUrl + "/Register/emailVerify";
            return $http.post(STATUS_URL, query, httpApiConfig);
        },
        sendEmail: function (query) {
            var EMAIL_URL = APP_CONFIG.apiRootUrl + "/Register/sendEmail";
            return $http.post(EMAIL_URL, query, httpApiConfig);
        }
    }
});
