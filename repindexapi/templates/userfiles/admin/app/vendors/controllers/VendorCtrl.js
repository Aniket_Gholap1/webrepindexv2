'use strict';

angular.module('app.vendors').controller('VendorCtrl', function ($scope, $state, $filter, VendorResources, DTOptionsBuilder) {



    var loadRoles = function () {
        console.log("API to bring roles")
        var roleRequest = VendorResources.roles();
        roleRequest.success(function (roleResponse) {
            var roles = roleResponse.result;
            $scope.role = $filter('filter')(roles, { role: 'Vendor' })[0];
            // var id = $scope.role_id.id;
        })
    }
    loadRoles();


    var loadVendors = function () {
        console.log("API to bring vendors")
        var VendorRequest = VendorResources.vendors();
        VendorRequest.success(function (VendorResponse) {
            if (VendorResponse.success) {
                $scope.vendors = VendorResponse.result;

            }
            // else if ($scope.admins.status == 'inactive') {
            //     $scope.inActiveUser = true;
            // }

        })
    }

    loadVendors();

    $scope.setVendor = function (vendor, index) {
        $scope.currentVendorIndex = index;
        $scope.currentVendor = vendor;
    }

    $scope.changeStatus = function (vendor) {
        var query = {};
        query.id = vendor.id;
        var changeStatusRequest = VendorResources.changeStatus(query);
        changeStatusRequest.success(function (changeStatusResponse) {

            loadVendors();

        })
    }

    $scope.emailVerify = function (vendor) {
        var query = {};
        query.id = vendor.id;
        var emailVerifyRequest = VendorResources.emailVerify(query);
        emailVerifyRequest.success(function (emailVerifyResponse) {
            loadVendors();
        })
    }

    $scope.sendEmail = function (vendor) {
        var query = {};
        query.id = vendor.id;
        var sendEmailRequest = VendorResources.sendEmail(query);
        sendEmailRequest.success(function (sendEmailResponse) {
            if (sendEmailResponse.success) {
                $scope.result = sendEmailResponse.result;
            }
            else {
                $scope.result = sendEmailResponse.result;

            }
        })
    }

    $scope.addVendor = function () {
        $scope.newVendor = {};
        $('#addVendorPopup').modal('show');
    }

    $scope.editVendor = function () {
        if ($scope.currentVendor != null) {
            $scope.newVendor = $scope.currentVendor;
            $('#addVendorPopup').modal('show');
        }
        // else {
        //     $scope.modalInstance = $modal.open({
        //         templateUrl: 'app/modules/data-gov/views/noSelection.modal.view.html',
        //         controller: 'removeModalCtrl',
        //         controllerAs: 'vm',
        //         resolve: {
        //             model: function () { return 'Vendor' },
        //             action: function () { return 'Edit'; }
        //         }
        //     });
        // }
    }

    $scope.deleteVendor = function () {
        if ($scope.currentVendor != null) {
            $('#deleteVendorPopup').modal('show');
        }
    }

    $scope.vendorDelete = function () {
        console.log("API to delete Vendor")
        var query = {};
        query.id = $scope.currentVendor.id;
        var vendorRequest = VendorResources.deleteVendor(query);
        vendorRequest.success(function (vendorResponse) {
            if (vendorResponse.success) {
                loadVendors();
                $('#deleteVendorPopup').modal('hide');
                $scope.success = true;
                $scope.vendors = vendorResponse.result;
            }
            else {
                $('#deleteVendorPopup').modal('hide');
                $scope.failed = true;
            }
        })
    }

    $scope.submit = function (newVendor) {
        if ($scope.vendorAction == 'Add') {
            var addVendorReq = VendorResources.addVendor(newVendor);
            addVendorReq.success(function (addVendorResponse) {
                if (addVendorResponse.success) {
                    loadVendors();
                    $scope.success = true;
                    $scope.result = addVendorResponse.result;
                }
                else {
                    $scope.failed = true;
                }
            })
        }
        if ($scope.vendorAction == 'Edit') {
            var editVendorReq = VendorResources.editVendor(newVendor);
            editVendorReq.success(function (editVendorResponse) {
                if (editVendorResponse.success) {
                    loadVendors();
                    $scope.success = true;
                    $scope.result = editVendorResponse.result;
                }
                else {
                    $scope.failed = true;
                }

            })
        }
        $scope.newAdmin = {};
        $scope.currentAdminIndex = null;
        $('#addAdminPopup').modal('hide');
    }

    $scope.cancel = function () {
        $scope.currentVendor = null;
        $scope.newVendor = {};
        $scope.currentVendor = null;
        $('#addVendorPopup').modal('hide');
        $('#deleteVendorPopup').modal('hide');
    }

    $scope.standardOptions = DTOptionsBuilder.newOptions()
        .withOption('bFilter', true)
        .withOption('bLengthChange', true)
        .withOption('bAutoWidth', false)
        .withOption('bPaginate', true)
        .withOption('bInfo', false)
        .withOption('bInfo', false)
        .withDOM("<'dt-toolbar'<'col-xs-12 col-sm-6'l><'col-sm-6 col-xs-12 hidden-xs'f>r>" +
        "t" +
        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>")
        // .withDOM('frtrip')
        .withLanguage({
            "sSearch": "<span class='input-group-addon input-sm'><i class='glyphicon glyphicon-search'></i></span> "
        })
        .withBootstrap();

});