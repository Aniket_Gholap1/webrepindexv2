"use strict";

angular.module('app.vendors', [ 'ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.vendors', ['ui.router'])
    .config(function ($stateProvider) {

        $stateProvider
            .state('app.vendors', {
                url: '/vendors',
                data: {
                    title: 'Vendor'
                },
                views: {
                    "content@app": {
                        templateUrl: 'app/vendors/views/vendors.html',
                        controller: 'VendorCtrl'
                    }
                }
            })
    });
