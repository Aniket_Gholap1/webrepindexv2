"use strict";

angular.module('app.email', [ 'ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.email', ['ui.router'])
.config(function ($stateProvider) {

    $stateProvider
        .state('app.email', {
            url: '/email',
            data: {
                title: 'Blank'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/email/views/email.html',
                    controller: 'EmailCtrl'
                }
            }
        })
});
