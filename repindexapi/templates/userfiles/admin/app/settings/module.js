"use strict";

angular.module('app.settings', [ 'ui.router', 'datatables', 'datatables.bootstrap']);

angular.module('app.settings', ['ui.router'])
.config(function ($stateProvider) {

    $stateProvider
        .state('app.settings', {
            url: '/settings',
            data: {
                title: 'settings'
            },
            views: {
                "content@app": {
                    templateUrl: 'app/settings/views/settings.html',
                    controller: 'SettingCtrl'
                }
            }
        })
});
