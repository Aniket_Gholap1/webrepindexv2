from django.db import models

# Create your models here.
from django.contrib.auth.models import User, UserManager
from django.db import models



# Create your models here.
class UserTypeInfo(models.Model):
    user_type=models.CharField(max_length=32)
    


class UserInfo(models.Model):
    userobject=models.ForeignKey(User,on_delete=models.CASCADE,default="")
    userid=models.CharField(max_length=16, null=True, blank=True)
    mobile=models.CharField(max_length=16, null=True, blank=True)
    dob=models.DateField(auto_now_add=True)
    created_at=models.DateTimeField(auto_now_add=True)
    user_type_id=models.ForeignKey(UserTypeInfo)
    status=models.IntegerField(default=1)

    class Meta:
        db_table = 'userinfo'


class Country(models.Model):
    name = models.CharField(max_length=64)
    short_code = models.CharField(max_length=16)
    mobile_code = models.CharField(max_length=16,null=True)
    status = models.IntegerField(blank=True)


    class Meta:
        db_table = 'country'

    def __unicode__(self):

        return str(u''.join((self.name)).encode('utf-8').strip())

class Address(models.Model):
    address_line1 = models.CharField(max_length=1024,blank=True,null=True)
    address_line2 = models.CharField(max_length=1024,blank=True,null=True)
    city = models.CharField(max_length=1024,blank=True,null=True)
    zipcode = models.CharField(max_length=16,blank=True,null=True)
    floor_number = models.IntegerField(blank=True,null=True,default=0)
    country = models.ForeignKey(Country, models.DO_NOTHING, null=True, blank=True, default=None)

    class Meta:
        db_table = 'address'

    def __unicode__(self):
        return str(self.feature_name)


class Projects(models.Model):
    name=models.CharField(max_length=128)
    created_by=models.ForeignKey(UserInfo,models.CASCADE, related_name="project_created_by")
    updated_by=models.ForeignKey(UserInfo,models.CASCADE, related_name="project_updated_by")
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on =models.DateTimeField(auto_now=True)
    status=models.IntegerField()


class Folder(models.Model):
    name=models.CharField(max_length=128)
    parent_folder_id=models.ForeignKey('self',null=True)
    created_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="folder_created_by")
    updated_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="folder_updated_by")
    status = models.IntegerField()


class QueryList(models.Model):
    project_id=models.ForeignKey(Projects)
    name=models.CharField(max_length=128)
    created_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="querylist_created_by")
    updated_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="querylist_updated_by")
    status = models.IntegerField()


class FolderType(models.Model):
    type = models.CharField(max_length=128)  
    folder = models.ForeignKey(Folder,related_name='folders')
    project = models.ForeignKey(Projects, models.CASCADE)


class TextList(models.Model):
    project_id=models.ForeignKey(Projects)
    name=models.CharField(max_length=128)
    folder_id=models.ForeignKey(Folder,null=True)
    created_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="textlist_created_by")
    updated_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="textlist_updated_by")
    status = models.IntegerField()
    has_splits=models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on =models.DateTimeField(auto_now=True)

"""
class ConcordanceList(models.Model):
    project_id=models.ForeignKey(Projects)
    name=models.CharField(max_length=128)
    folder_id=models.ForeignKey(Folder,null=True)
    created_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="concordance_created_by")
    updated_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="concordance_updated_by")
    status = models.IntegerField()
"""

class TextListDetails(models.Model):
    text_list_id=models.ForeignKey(TextList)
    file_content=models.TextField(max_length=65535)
    file_content_plain_text=models.TextField(max_length=65535)
    created_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="textlistdetails_created_by")
    updated_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="textlistdetails_updated_by")
    status = models.IntegerField()

    def __unicode__(self):
        return self.file_content_plain_text


class TextListSplits(models.Model):
    text_list_id=models.ForeignKey(TextList)
    next_content=models.ForeignKey('self')
    file_content=models.TextField(max_length=65535)
    file_content_plain_text=models.TextField(max_length=65535)
    created_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="textlistsplits_created_by")
    updated_by = models.ForeignKey(UserInfo, models.CASCADE, related_name="textlistsplits_updated_by")
    status = models.IntegerField()







class QueryTypeInfo(models.Model):
    name=models.CharField(max_length=32) #Local,Remote
    label=models.CharField(max_length=32)
    status = models.IntegerField()
 

class ResultTypeInfo(models.Model):
    name=models.CharField(max_length=32)#[text_list,concordance_lines,frequency_counts,cluster_pairs,concept_list_self_concordance]
    label=models.CharField(max_length=32)#[Text List,Concordance Lines,Frequency counts,cluster pairs,Concept list self concordance]
    status = models.IntegerField()


class ResultTypeParameterInfo(models.Model):
    result_type_info=models.ForeignKey(ResultTypeInfo, models.CASCADE, related_name="result_type_info")

    label=models.CharField(max_length=32)
    name=models.CharField(max_length=32)#int,char,string
    control_type=models.CharField(max_length=32)#combobox,textarea,tickbox,radiobutton,timepicker
    order = models.IntegerField()
    status = models.IntegerField()
    data_type = models.CharField(max_length=32)


class ResultTypeParameterIntValue(models.Model):
    result_type_parameter_info=models.ForeignKey(ResultTypeParameterInfo, models.CASCADE, related_name="result_type_parameter_int_value")
    query_list=models.ForeignKey(QueryList, models.CASCADE, related_name="query_list_int_value")
    value=models.IntegerField()



class ResultTypeParameterFloatValue(models.Model):
    result_type_parameter_info=models.ForeignKey(ResultTypeParameterInfo, models.CASCADE, related_name="result_type_parameter_float_value")
    query_list=models.ForeignKey(QueryList, models.CASCADE, related_name="query_list_double_value")
    value=models.FloatField()



class ResultTypeParameterStringValue(models.Model):
    result_type_parameter_info=models.ForeignKey(ResultTypeParameterInfo, models.CASCADE, related_name="result_type_parameter_string_value")
    query_list=models.ForeignKey(QueryList, models.CASCADE, related_name="query_list_string_value")
    value=models.CharField(max_length=256)


class QuerySettings(models.Model):
    query_type_info=models.ForeignKey(QueryTypeInfo, models.CASCADE, related_name="query_settings_query_type_info")
    result_type_info=models.ForeignKey(ResultTypeInfo, models.CASCADE, related_name="query_settings_result_type_info")
    query_list=models.ForeignKey(QueryList, models.CASCADE, related_name="query_settings_query_string_value")
    from_date =models.DateField()
    to_date=models.DateField()


class QueryListFiles(models.Model):
    query_list=models.ForeignKey(QueryList, models.CASCADE, related_name="query_list_files_string_value")
    text_list_id=models.ForeignKey(TextList,models.CASCADE,related_name="query_list_files")




class ConceptList(models.Model):
    name=models.CharField(max_length=64)
    project_id = models.ForeignKey(Projects)
    status = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)




class ConceptListValue(models.Model):
    concept_list = models.ForeignKey(ConceptList, models.CASCADE, related_name="concept_list_value")
    umbrella=models.CharField(max_length=64)
    status= models.IntegerField()


class ConceptListUmbrellaValues(models.Model):
    concept_list_value = models.ForeignKey(ConceptListValue, models.CASCADE, related_name="concept_list_umbrella_value")
    term=models.CharField(max_length=64)



class FrequencyCount(models.Model):
    name=models.CharField(max_length=64)
    query_list = models.ForeignKey(QueryList, models.CASCADE, related_name="freq_count_query_list")
    project_id = models.ForeignKey(Projects)
    status = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)


class FrequencyCountValues(models.Model):
    frequency_count = models.ForeignKey(FrequencyCount, models.CASCADE, related_name="freq_count")
    word = models.CharField(max_length=32)
    count = models.IntegerField()
    lines = models.CharField(max_length=32)

class SelectedTextlist(models.Model):
    query_list = models.ForeignKey(QueryList, models.CASCADE, related_name="query_list_text_list")
    project_id = models.ForeignKey(Projects)
    text_list= models.ForeignKey(TextList, models.CASCADE, related_name="selected_text_list")


class StopList(models.Model):
    name=models.CharField(max_length=64)
    project_id = models.ForeignKey(Projects)
    status = models.IntegerField()
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)






class StopListValue(models.Model):
    stop_list = models.ForeignKey(StopList, models.CASCADE, related_name="stop_list_value")
    value=models.CharField(max_length=64)
    status= models.IntegerField()



class ConceptListProperties(models.Model):
    isdefault=models.IntegerField()
    concept_list=models.ForeignKey(ConceptList,models.CASCADE,related_name="concept_list_properties")



