# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('repindexapp', '0005_auto_20170924_0549'),
    ]

    operations = [
        migrations.AlterField(
            model_name='address',
            name='country',
            field=models.ForeignKey(default=None, to_field=django.db.models.deletion.DO_NOTHING, blank=True, to='repindexapp.Country', null=True),
        ),
        migrations.AlterField(
            model_name='conceptlistproperties',
            name='concept_list',
            field=models.ForeignKey(related_name='concept_list_properties', to='repindexapp.ConceptList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='conceptlistumbrellavalues',
            name='concept_list_value',
            field=models.ForeignKey(related_name='concept_list_umbrella_value', to='repindexapp.ConceptListValue', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='conceptlistvalue',
            name='concept_list',
            field=models.ForeignKey(related_name='concept_list_value', to='repindexapp.ConceptList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='folder',
            name='created_by',
            field=models.ForeignKey(related_name='folder_created_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='folder',
            name='updated_by',
            field=models.ForeignKey(related_name='folder_updated_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='foldertype',
            name='project',
            field=models.ForeignKey(to='repindexapp.Projects', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='frequencycount',
            name='query_list',
            field=models.ForeignKey(related_name='freq_count_query_list', to='repindexapp.QueryList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='frequencycountvalues',
            name='frequency_count',
            field=models.ForeignKey(related_name='freq_count', to='repindexapp.FrequencyCount', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='projects',
            name='created_by',
            field=models.ForeignKey(related_name='project_created_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='projects',
            name='updated_by',
            field=models.ForeignKey(related_name='project_updated_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='querylist',
            name='created_by',
            field=models.ForeignKey(related_name='querylist_created_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='querylist',
            name='updated_by',
            field=models.ForeignKey(related_name='querylist_updated_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='querylistfiles',
            name='query_list',
            field=models.ForeignKey(related_name='query_list_files_string_value', to='repindexapp.QueryList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='querylistfiles',
            name='text_list_id',
            field=models.ForeignKey(related_name='query_list_files', to='repindexapp.TextList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='querysettings',
            name='query_list',
            field=models.ForeignKey(related_name='query_settings_query_string_value', to='repindexapp.QueryList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='querysettings',
            name='query_type_info',
            field=models.ForeignKey(related_name='query_settings_query_type_info', to='repindexapp.QueryTypeInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='querysettings',
            name='result_type_info',
            field=models.ForeignKey(related_name='query_settings_result_type_info', to='repindexapp.ResultTypeInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='resulttypeparameterfloatvalue',
            name='query_list',
            field=models.ForeignKey(related_name='query_list_double_value', to='repindexapp.QueryList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='resulttypeparameterfloatvalue',
            name='result_type_parameter_info',
            field=models.ForeignKey(related_name='result_type_parameter_float_value', to='repindexapp.ResultTypeParameterInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='resulttypeparameterinfo',
            name='result_type_info',
            field=models.ForeignKey(related_name='result_type_info', to='repindexapp.ResultTypeInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='resulttypeparameterintvalue',
            name='query_list',
            field=models.ForeignKey(related_name='query_list_int_value', to='repindexapp.QueryList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='resulttypeparameterintvalue',
            name='result_type_parameter_info',
            field=models.ForeignKey(related_name='result_type_parameter_int_value', to='repindexapp.ResultTypeParameterInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='resulttypeparameterstringvalue',
            name='query_list',
            field=models.ForeignKey(related_name='query_list_string_value', to='repindexapp.QueryList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='resulttypeparameterstringvalue',
            name='result_type_parameter_info',
            field=models.ForeignKey(related_name='result_type_parameter_string_value', to='repindexapp.ResultTypeParameterInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='selectedtextlist',
            name='query_list',
            field=models.ForeignKey(related_name='query_list_text_list', to='repindexapp.QueryList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='selectedtextlist',
            name='text_list',
            field=models.ForeignKey(related_name='selected_text_list', to='repindexapp.TextList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='stoplistvalue',
            name='stop_list',
            field=models.ForeignKey(related_name='stop_list_value', to='repindexapp.StopList', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='textlist',
            name='created_by',
            field=models.ForeignKey(related_name='textlist_created_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='textlist',
            name='updated_by',
            field=models.ForeignKey(related_name='textlist_updated_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='textlistdetails',
            name='created_by',
            field=models.ForeignKey(related_name='textlistdetails_created_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='textlistdetails',
            name='updated_by',
            field=models.ForeignKey(related_name='textlistdetails_updated_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='textlistsplits',
            name='created_by',
            field=models.ForeignKey(related_name='textlistsplits_created_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
        migrations.AlterField(
            model_name='textlistsplits',
            name='updated_by',
            field=models.ForeignKey(related_name='textlistsplits_updated_by', to='repindexapp.UserInfo', to_field=django.db.models.deletion.CASCADE),
        ),
    ]
