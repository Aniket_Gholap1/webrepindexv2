from django.conf.urls import url
from django.contrib import admin
from repindexapp import views
from django.conf.urls import include, url
urlpatterns = [
    url(r'^Register/$', views.clientregsiter),
    url(r'^login/$', views.Login),
    url(r'^getallclient/$', views.getAllClient),
    url(r'^deleteUser/$', views.deleteUser),
    url(r'^inactiveUser/(?P<user_id>\d+)/$', views.inactiveUser),
    url(r'^getuserbyid/(?P<user_id>\d+)/$', views.getuserbyid),
    url(r'^activateUser/(?P<user_id>\d+)/$', views.activateUser),
    url(r'^updateClientInfo/(?P<id>\d+)/$', views.updateClientInfo),
    url(r'^addproject/(?P<user_id>\d+)/$', views.addProject),
    url(r'^getproject/(?P<user_id>\d+)/$', views.getProject),
    url(r'^renameproject/$', views.renameproject),
    url(r'^deleteproject/$', views.deleteproject),
    url(r'^getprojectDetails/(?P<project_id>\d+)/$', views.getprojectDetails),
    url(r'^addFolder/(?P<project_id>\d+)/(?P<user_id>\d+)/$', views.addFolder),
    url(r'^renameFolder/(?P<folder_id>\d+)/$', views.renamefolder),
    url(r'^deleteFolder/(?P<folder_id>\d+)/$', views.deletefolder),
    url(r'^Savetextlist/(?P<project_id>\d+)/(?P<user_id>\d+)/(?P<folder_id>\d+)$', views.Savetextlist),
    url(r'^saveConceptlist/(?P<project_id>\d+)/(?P<user_id>\d+)/$', views.saveConceptlist),
    url(r'^concordanceline/(?P<project_id>\d+)/$', views.concordanceline),
    url(r'^importstoplist/(?P<project_id>\d+)/$', views.importstoplist),
    url(r'^renameconcept/$', views.renameconcept),
    url(r'^deleteconcept/$', views.deleteconcept),
]


