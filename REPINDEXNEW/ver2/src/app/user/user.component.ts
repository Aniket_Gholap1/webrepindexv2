 import { Component, Input, Output ,  OnInit , EventEmitter } from '@angular/core';

import { repindexService } from '../repindex.service';
 import { UserService } from './user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
    selector: 'child-comp',
  templateUrl: './user.component.html',
  
})


export class UserComponent implements OnInit{
   
  selectedCity:any="Concordance";
  projectData: any = {};
  conscurdance: any = {};
  clusterdata: any = {};
  textdata: any = [];
  docId: any = [];
  concordanceObject :any=[];
  public textliststatus :boolean=false;
  public constatus: boolean = true;
  public ids:string;
  public self: boolean = false;
  public cluster: boolean = false;
  projectName;
  public categoryTable: any;
  public showTable: boolean = false;
  public selected_ids:any;
  public txt_toi:any;
  public getconcept:any=[];
  public selfcon:any={};
 constructor( private router: Router, private UserService: UserService,private activeRoute: ActivatedRoute) {
 this.router=router;
  
}

ngOnInit(): void {
           
        
  
   
           localStorage.setItem("url",window.location.href );
         localStorage.setItem("type","concordance")
         localStorage.removeItem("conceptlist");
        localStorage.removeItem("selectedids");
        this.activeRoute.queryParams.subscribe((params: Params) => {
        var that = this;       
        var  projectId = params['projectId'];
         localStorage.setItem('projectId',projectId );  
        localStorage.setItem('projectName', params['project']);  
         
           if( localStorage.getItem("counter")=="0"){
          this.hello();
          }
          
       localStorage["counter"] = "1";
           var dd=localStorage.getItem('projectId');  
           this.UserService.gettextlist(dd).subscribe(data => {
           this.textdata=data;
         
        //  this.categoryTable = $('#categoryTable').DataTable({
        //   responsive: false,
        //   "pagingType": "full_numbers",
        // });
         
           console.log( "textdata",this.textdata );
        });

         this.UserService.getconcept(projectId).subscribe(data => {
        
          this.getconcept=data;
          console.log("this.getconcept",this.getconcept)
        });
          
          
      }) 

      
  }

  hello(){
       location.reload();
       
  }

 onChange($event:any)
 {
   console.log("fff",this.selectedCity);
   if(this.selectedCity=="Concordance")
   {
   console.log("in cordance")
   this.constatus=true;
   this.self=false;
   this.cluster=false;
   console.log(this.constatus)
   localStorage.setItem("type","concordance")
   }
   if(this.selectedCity=='self')
   {
        this.constatus=false;
        this.self=true;
        this.cluster=false;
        localStorage.setItem("type","self")
   }
   if(this.selectedCity=='cluster')
   {
      this.constatus=false;
      this.self=false;
      this.cluster=true;
       localStorage.setItem("type","cluster")
   }
   if(this.selectedCity=='Frequency')
   {
     console.log("frquncy")
   }
 }
 concordanceapi()
 {
   

   console.log("concorncace api",  localStorage.getItem("type"));

   if(localStorage.getItem("type")=="cluster")
   {
         var pid=localStorage.getItem('projectId'); 
         var uid=localStorage.getItem('userId')
         var cluster = (<HTMLInputElement>document.getElementById("txtarea_cluster_pairs")).value;
         var ids=localStorage.getItem("selectedids");
         var selected_ids=ids + ",";
         this.clusterdata.txtarea_cluster_pairs=cluster;
         this.clusterdata.selected_ids=selected_ids;
         console.log(this.clusterdata)
          this.UserService.clusterSearch(this.clusterdata,pid,uid).subscribe(data => {
           this.concordanceObject=data.results;

            if(this.concordanceObject.length!=0)
           {
           this.showTable=true;
           }
           console.log("ng onit called");
          console.log( "data",data );
        });

   }
   else if(localStorage.getItem("type")=="self")
   {
         var pid=localStorage.getItem('projectId'); 
         var uid=localStorage.getItem('userId')
         var cid=localStorage.getItem("conceptlist");
         var ids=localStorage.getItem("selectedids");
         var selected_ids=ids + ",";

         console.log(typeof(selected_ids))
          this.selfcon.selected_ids=selected_ids;
          this.selfcon.hf_cmb_concept_lists=cid;
          console.log("api data",this.selfcon);
          
          this.UserService.selfconcordancesearch(this.selfcon,pid,uid).subscribe(data => {
           this.concordanceObject=data.results;
          console.log( "self concordancedata",data );
           
            if(this.concordanceObject.length!=0)
           {
           this.showTable=true;
           }
        });
         
   }
   else if (localStorage.getItem("type")=="concordance")
   {
       console.log("write a[i for con")
     
      var lst_toi = (<HTMLInputElement>document.getElementById("lst_toi")).value;
      var ids=localStorage.getItem("selectedids");
      var selected_ids=ids + ",";
      var txt_toi=lst_toi;
      var txt_cat="and"
      this.conscurdance.selected_ids=selected_ids;
      this.conscurdance.txt_toi=txt_toi;
      this.conscurdance.txt_cat=txt_cat;
      console.log(this.conscurdance);
       var pid=localStorage.getItem('projectId'); 
         this.UserService.concurdanceSearch(this.conscurdance,pid).subscribe(data => {
           this.concordanceObject=data.results;
           console.log(this.concordanceObject.length)
           if(this.concordanceObject.length!=0)
           {
           this.showTable=true;
           }
           console.log("ng onit called");
          console.log( "data",data.results );
        });

    }  
   else
   {
       console.log("write api for frequency")

   }

 }
 selectedtextlist(ids)
 {
  

  
   

   if (this.docId.indexOf(ids) === -1) {
    this.docId.push(ids);
  }
  else {
    var index = this.docId.indexOf(ids);
    this.docId.splice(index, 1)
  }


   console.log(this.docId);
   for(var i=0;i<this.docId.length;i++)
   {

      
   }
    localStorage.setItem("selectedids",this.docId);
    
   
  }

  test(valeu){

  console.log("in test func",valeu);
  var res = valeu.substring(0, 4);
   return res;

  }
  left(value)
  {
       var t = value.split(" "); 
    if(value)  
  {  
       var  lastsevan=t[t.length - 7] +" "+t[t.length - 6]+" "+t[t.length - 5] +" "+t[t.length - 4]+" " +t[t.length - 3] + " "+ t[t.length - 
2]+" "+ t[t.length-1]       
   }    
 return lastsevan;
  }
  right(value)
  {
  var t = value.split(" ");
  if(value) 
  {    
   var  lastsevan=t[0]+" "+t[1]+" "+t[2]+" "+t[3]+" "+t[4]+" "+t[5]+" "+t[6]+" "+t[7];
   }  
    return lastsevan;
  }
  conceptlist(event:any)
  { 
    console.log(event);
     localStorage.setItem("conceptlist",event);
    
  }
 
 navigateToEditor(dd,leftv,rightv,toi){
   console.log(dd)
   
       localStorage.setItem("fileid",dd);
        localStorage.setItem("fileid",dd);
       localStorage.setItem("left",rightv);
       localStorage.setItem("right",leftv);
       localStorage.setItem("toi",toi);
       var strWindowFeatures = "location=yes,height=570,width=520,scrollbars=yes,status=yes";
       window.open("http://localhost:4200/#/editor?+fileid="+dd,"_blank",strWindowFeatures)
    // this.router.navigate(['/editor'],{ queryParams: { fileid: dd} });
     //setTimeout(function(){ window.location.reload() }, 300);
 }
 projectgo()
 {
    this.router.navigate(['/project']);
    setTimeout(function(){ window.location.reload() }, 40);
 }
 newquery()
 {
   //this.router.navigate(['/user']);
    setTimeout(function(){ window.location.reload() }, 25);
 }
}