import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { environment } from '../../environments/environment';


@Injectable()
export class UserService {  

  constructor( private http: Http) { }
 
    getData(id) { 
        
           
             return this.http.get(environment.baseUrl+"getprojectDetails/"+id)
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}
	gettextlist(pid) { 
        
           
             return this.http.get(environment.baseUrl+"getalltextlist/"+pid)
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}
	concurdanceSearch(obj,pid) { 
        
           
             return this.http.post(environment.baseUrl+"concordanceline/"+pid,JSON.stringify(obj))
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}
	clusterSearch(obj,pid,uid) { 
        
           
             return this.http.post(environment.baseUrl+"clusterquery/"+pid+"/"+uid+"/",JSON.stringify(obj))
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}
	getconcept(pid) { 
        
           
             return this.http.get(environment.baseUrl+"getconceptlist/"+pid)
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}
    selfconcordancesearch(obj,pid,uid)
    {
            return this.http.post(environment.baseUrl+"selfcordoncordance/"+pid+"/"+uid+"/",JSON.stringify(obj))
			.map((response: Response) => {	
				 						
				return response.json();			
			})


    }


   

 
	 

	  
}