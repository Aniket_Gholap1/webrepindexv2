import { Component } from '@angular/core';
import { repindexService } from '../repindex.service';
import { Router } from '@angular/router'; 
import { CommonModule } from '@angular/common';  
import { BrowserModule } from '@angular/platform-browser';
import { LoginService } from './login.service';

@Component({
  templateUrl: './login.component.html'
})


export class LoginComponent {

private validationStatus :boolean=false;
private usernameStatus :boolean=false;
private passwordStatus :boolean=false;
private usernameStatusError :boolean=false;
private passwordStatusError :boolean=false;
private loginfail :boolean=false;
private uusername;
  userdata: any = {};

 constructor( private router: Router,private LoginService: LoginService) {
 this.router=router;
 }
authenticate(name,password)
{
     
      this.userdata.username = name;
      this.userdata.password = password;
      this.LoginService.login(this.userdata).subscribe(data => {
       localStorage.setItem('username',data.username);
      if(data.user_type=="admin")  {
         localStorage.setItem('userId',data.user_id);
        
         localStorage.setItem('usertype',data.user_type);
         this.router.navigate(['/dashboard']);
         setTimeout(function(){ window.location.reload() }, 100);
      }
      else if(data.user_type=="user"){

          
         localStorage.setItem('userId',data.user_id);
         //localStorage.setItem('usertype',data.user_type);
           localStorage.setItem('counter',"0");
             localStorage.setItem("backProjectCounter","0" ); 
          this.router.navigate(['/project']);
      }  
      else
      {
        this.loginfail=true;
        this.router.navigate(['/login']);
      }
      });
 
 

}

 validateUsername(event:any) {  
    this.uusername = event.target.value;
         


       if(this.uusername){
              this.usernameStatus = true;
       this.usernameStatusError = false;
       }
       else{
      
        this.usernameStatus = false;
        this.usernameStatusError = true;
       }


        if(this.usernameStatus &&  this.passwordStatus){
      this.validationStatus = true;
       }
       else{
      this.validationStatus = false;
       }



  }

  validatePassword(event:any){
     
       var pass = event.target.value;
            
       if(pass){
      this.validationStatus = true;
       this.passwordStatus = true;
       this.passwordStatusError = false;
       }
       else{
       this.validationStatus = false;
        this.passwordStatus = false;
         this.passwordStatusError = true;
       }
       
  
       if(this.usernameStatus &&  this.passwordStatus){
      this.validationStatus = true;
       }
       else{
      this.validationStatus = false;
       }

      

  }

}