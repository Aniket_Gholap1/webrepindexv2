import { Injectable, } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { environment } from '../../environments/environment';


@Injectable()
export class LayoutService {  

  constructor( private http: Http) { }
 
    getData(id) { 
        
           
             return this.http.get(environment.baseUrl+"getprojectDetails/"+id)
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}

	addFolder(folder,uid,pid) { 
        

            
             return this.http.post(environment.baseUrl+"addFolder/"+pid+"/"+uid+"/",JSON.stringify(folder))
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}

	renameFolder(id,obj) {  
        

            
             return this.http.post(environment.baseUrl+"renameFolder/"+id+"/",JSON.stringify(obj))
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}

		deleteFolder(id,obj) {  
        

            
             return this.http.post(environment.baseUrl+"deleteFolder/"+id+"/",JSON.stringify(obj))
			.map((response: Response) => {	
				 						
				return response.json();			
			})
	}

    addNewTextList(uid,pid,fid,obj){
		     let formData:FormData = new FormData();
			 formData.append('text_file',obj);
			 let headers = new Headers();
		     headers.append('Content-Type', 'multipart/form-data');
             headers.append('Accept', 'application/json');
		     let options = new RequestOptions({ headers: headers });

				console.log(formData);

		return this.http.post(environment.baseUrl+"Savetextlist/"+pid+"/"+uid+"/"+fid, formData)
			.map((response: Response) => {	
				 						
				return response.json();			
			})

	}

	  addNewConceptList(obj){
		     let formData:FormData = new FormData();
			 formData.append('browse_concept_list',obj);
			 let headers = new Headers();
		     headers.append('Content-Type', 'multipart/form-data');
             headers.append('Accept', 'application/json');
		     let options = new RequestOptions({ headers: headers });

		 

		return this.http.post(environment.baseUrl+"saveConceptlist/"+localStorage.getItem('projectId')+"/"+localStorage.getItem('userId')+"/", formData)
			.map((response: Response) => {	
				 						
				return response.json();			
			})

	}

		  deleteConceptList(obj){
		    
		 

		return this.http.post(environment.baseUrl+"deleteconcept/",JSON.stringify(obj))
			.map((response: Response) => {	
				 						
				return response.json();			
			})

	}

	  renameConceptList(obj){
		    
		 

		return this.http.post(environment.baseUrl+"renameconcept/",JSON.stringify(obj))
			.map((response: Response) => {	
				 						
				return response.json();			
			})

	}

	
	
 
   

 
	 

	  
}