
 import { Component, Input, Output ,  OnInit , EventEmitter } from '@angular/core';
import { LayoutService } from './layouts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
 import { ModalDirective } from 'ng2-bootstrap/modal/modal.component';
 import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
   providers: [LayoutService],
   
   
})
export class FullLayoutComponent implements OnInit {
  
  public disabled: boolean = false;
  public projectName;
  public username: string;
  public userName: string;
  public project: string;
  public status: {isopen: boolean} = {isopen: false};
  conceptlists: any = {};
  public concept: any = [];
  public textlistfolders: any = [];
newFolderData: any = {};
renameFolderData: any = {};
deleteFolderData: any = {};
newTextListData: any = {};
conceptDeleteData: any = {};
conceptRenameData: any = {};
textListFile;
public loader: boolean = false;
  public uploaded: any = [];
   public conceptListFile: any = [];
 
   constructor( private router: Router, private UserService: LayoutService,private activeRoute: ActivatedRoute) {
 this.router=router;
}

  public toggled(open: boolean): void {
    
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit(): void {   
     
  
   
    this.userName = localStorage.getItem("username");
     this.username = localStorage.getItem("usertype");
     this.project=localStorage.getItem("addproject");
     var pid = localStorage.getItem('projectId');
      this.projectName = localStorage.getItem('projectName');
       
       this.UserService.getData(pid).subscribe(data => {
          

           this.concept = data.concept_lists;
           this.textlistfolders=data.text_list_folders;
          console.log(this.concept);
        });
         
        console.log();
        if(localStorage.getItem("addproject")=="beforeproject")
         {
           this.hide();
         }
         
      
  }
  
  renameTextListFolder(event){ 
     document.getElementById("renameFolder").click();
     var i =  (<HTMLInputElement>document.getElementById("hiddenId-of")).value;
      var name =  (<HTMLInputElement>document.getElementById("textOfFolder")).value;
       (<HTMLInputElement>document.getElementById("folderRenameId")).value=i;
        (<HTMLInputElement>document.getElementById("folderRenameText")).value=name;

     
    
  }

  closem1(){
      document.getElementById("m1").style.display = "none";

  }
   closem3(){
      document.getElementById("m3").style.display = "none";

  }

   closem2(){
      document.getElementById("m2").style.display = "none";

  }

  closeM5(){
     document.getElementById("m5").style.display = "none";
  }
  closem4(){
      document.getElementById("m4").style.display = "none";

  }

  renameFolder(){
     var text =  (<HTMLInputElement>document.getElementById("folderRenameText")).value;
     var id =  (<HTMLInputElement>document.getElementById("folderRenameId")).value;
     
        
      this.renameFolderData.folder_name=text;
      this.UserService.renameFolder(id,this.renameFolderData).subscribe(data => {
        setTimeout(function(){ 

             location.reload();
           },1000);
      });
    

  }

  addNewTextList(){ 
    document.getElementById("addTextList").click();
    var i =  (<HTMLInputElement>document.getElementById("hiddenId-of")).value;
     (<HTMLInputElement>document.getElementById("id-hidden-for-new-text-list")).value=i; 
     
  }

  public fileChangeEvent(event){ 
    
       let fileList: FileList = event.target.files;
       let file: File = fileList[0];
       this.uploaded = fileList[0];
    }

    fileChangeEvent2(event){
       let fileList: FileList = event.target.files;
       let file: File = fileList[0];
       this.conceptListFile = fileList[0];
    }

  addTextList(){
        
      var x =  (<HTMLInputElement>document.getElementById("newTextListFile")).value;
      var id =  (<HTMLInputElement>document.getElementById("id-hidden-for-new-text-list")).value;
     
       
     this.UserService.addNewTextList(localStorage.getItem('userId'),localStorage.getItem('projectId'),id,this.uploaded).subscribe(data => {
        setTimeout(function(){ 

             location.reload();
           },1000);
      }); 

  }

  newConceptListImport(){ 
    document.getElementById("addConceptList").click();
  }

  addConceptList(){
        this.loader=true;
        this.UserService.addNewConceptList( this.conceptListFile).subscribe(data => {
          console.log(data);
        setTimeout(function(){ 

             location.reload();
           },100);
      }); 
  }
  hide(){
  
   document.getElementById("toglerbtn").click();
   document.getElementById("toglerbtn").style.display = "none";

  }

  showConceptListMenu(event){
       document.getElementById("m4").style.display = "block";
       var target = event.target || event.srcElement || event.currentTarget;
        var name = target.id.substring(0, target.id.indexOf('@')); 
      var id  = target.id.substring(target.id.indexOf("@") + 1);
        (<HTMLInputElement>document.getElementById("hiddenId-of-concept-list")).value=id;
       (<HTMLInputElement>document.getElementById("textOfConceptList")).value=name;

    return false;
  }

  logout()
  {
   localStorage.clear();
  }

  
 demo1(){
    document.getElementById("m1").style.display = "block";
    
    return false;
 }

  demo2(){
     
     document.getElementById("m2").style.display = "block";
 
  
    return false;
 }

  
    demo3(event){
   
      document.getElementById("m3").style.display = "block";
 
     var target = event.target || event.srcElement || event.currentTarget;
       
      var id = target.id.substring(0, target.id.indexOf('@')); 
      var name  = target.id.substring(target.id.indexOf("@") + 1);

      (<HTMLInputElement>document.getElementById("hiddenId-of")).value=id;
       (<HTMLInputElement>document.getElementById("textOfFolder")).value=name;
    return false;
 }

addNewFolderToTextList(){

    document.getElementById("uniq").click();

}
createNewFolder(){ 
      this.newFolderData.folder_name = (<HTMLInputElement>document.getElementById("text-list-new-folder-name")).value;
    
 
       
      this.UserService.addFolder( this.newFolderData, localStorage.getItem("userId"), localStorage.getItem("projectId")).subscribe(data => {
            setTimeout(function(){ 

             location.reload();
           },1000);

        });

}

deleteTextListFolder(){  
   document.getElementById("deleteFolder").click();
   var i =  (<HTMLInputElement>document.getElementById("hiddenId-of")).value;
       (<HTMLInputElement>document.getElementById("folderDeleteId")).value=i;
}



deleteFolder(){

      var id =  (<HTMLInputElement>document.getElementById("folderDeleteId")).value;
      
      
      this.UserService.deleteFolder(id,this.deleteFolderData).subscribe(data => {
        setTimeout(function(){ 

             location.reload();
           },1000);
      });
    

}  

showConceptListMenuUmbrella(event){ 
   

      var target = event.target || event.srcElement || event.currentTarget;
      
             
      var umbrella = target.id.substring(0, target.id.indexOf('@')); 
     var terms = target.id.substring(target.id.indexOf("@") + 1); 
  
      (<HTMLInputElement>document.getElementById("hidenTermsOfUmbrella")).value= terms;
(<HTMLInputElement>document.getElementById("hidenUmbrella")).value= umbrella;
          document.getElementById("m5").style.display = "block";
        
      return false;

}

insertIntosearch(){
     var terms =  (<HTMLInputElement>document.getElementById("hidenTermsOfUmbrella")).value;
     var umbrella = (<HTMLInputElement>document.getElementById("hidenUmbrella")).value;
     (<HTMLInputElement>document.getElementById("lst_toi")).value=terms;
     (<HTMLInputElement>document.getElementById("lst_toi1")).value=umbrella;
}

insertintoand(){
    var terms =  (<HTMLInputElement>document.getElementById("hidenTermsOfUmbrella")).value;
     (<HTMLInputElement>document.getElementById("and")).value=terms;
}
renameConceptList(){
  document.getElementById("renameConceptList").click();
   var i =  (<HTMLInputElement>document.getElementById("hiddenId-of-concept-list")).value;
   var name =  (<HTMLInputElement>document.getElementById("textOfConceptList")).value;
       (<HTMLInputElement>document.getElementById("pop-up-concpt-id")).value=i;
    (<HTMLInputElement>document.getElementById("pop-up-concept-name")).value=name;
    // conceptRenameData
}

renameeConceptList(){
    var id = (<HTMLInputElement>document.getElementById("pop-up-concpt-id")).value;
    var name = (<HTMLInputElement>document.getElementById("pop-up-concept-name")).value;
    this.conceptRenameData.concept_id=id;
     this.conceptRenameData.rename_list=name;
     this.UserService.renameConceptList(  this.conceptRenameData).subscribe(data => {
        setTimeout(function(){ 

             location.reload();
           },1000);
      });

}

deleteConceptList(){
   document.getElementById("deleteConceptList").click();
   var i =  (<HTMLInputElement>document.getElementById("hiddenId-of-concept-list")).value;
   var name =  (<HTMLInputElement>document.getElementById("textOfConceptList")).value;
    (<HTMLInputElement>document.getElementById("delete-concept-list-hidden-pop")).value=i;
}

deleteeConceptList(){
  this.conceptDeleteData.concept_id =  (<HTMLInputElement>document.getElementById("delete-concept-list-hidden-pop")).value;
  this.UserService.deleteConceptList(  this.conceptDeleteData ).subscribe(data => {
        setTimeout(function(){ 

             location.reload();
           },1000);
      });
}
allClose(){
  /*
  document.getElementById("m1").style.display = "none";
  document.getElementById("m3").style.display = "none";
   document.getElementById("m2").style.display = "none";*/
   
}
newQueryPgae()
{
   var url=localStorage.getItem("url");
  
   console.log("navigate to project",url);
   window.location.href=url;
  setTimeout(function(){ window.location.reload() }, 15);
}
 }  
